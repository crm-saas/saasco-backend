const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyJobCandidateSchema = new mongoose.Schema({
    companyJobId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyJobs"
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyUsers"
    },
    isPinned:{
        type: Boolean
    },
    notes: {
        type: String,
        required: false,
    },
    applicationAnswers:{
        type: Array
    },
});

module.exports = mongoose.model('companyJobCandidates', companyJobCandidateSchema);