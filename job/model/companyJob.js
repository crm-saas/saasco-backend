const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyJobSchema = new mongoose.Schema({
    companyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companies"
    },
    industryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Industries"
    },
    jobTypeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "jobTypes"
    },
    jobModalityId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "jobModalities"
    },
    jobScheduleId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "jobSchedules"
    },
    locationId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "locations"
    },
    requiredSkills: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "skills"
    }],
    secondarySkills: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "skills"
    }],
    recruiters: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyUsers"
    }],
    name: {
        type: String,
        required: false,
    },
    description: {
        type: String,
        required: false,
    },
    minSalaray: {
        type: Number
    },
    maxSalary: {
        type: Number
    },
    requiredExperience:{
        type: Boolean
    },
    keyWords:{
        type: Array
    },
    applicationQuestions:{
        type: Array
    },
    interviewQuestions:{
        type: Array
    },
    views:{
        type: Number
    },
    isPromoted:{
        type: Boolean
    }
});

module.exports = mongoose.model('companyJobs', companyJobSchema);