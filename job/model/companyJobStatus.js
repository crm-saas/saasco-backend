const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyJobStatusesSchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('companyJobStatuses', companyJobStatusesSchema);