const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyJobCandidateStatusHistorySchema = new mongoose.Schema({
    companyJobCanditateId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyJobCandidates"
    },
    companyJobCandidateStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyJobCandidateStatuses"
    },
    createdBy:{
        type: String
    },
    description: {
        type: String,
        required: false,
    },
    creationDate:{
        type: Date
    },
});

module.exports = mongoose.model('companyJobCandidateStatusHistories', companyJobCandidateStatusHistorySchema);