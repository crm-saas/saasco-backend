const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyJobCandidateStatusesSchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('companyJobCandidateStatuses', companyJobCandidateStatusesSchema);