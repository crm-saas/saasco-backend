const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyJobInterviewSchema = new mongoose.Schema({
    companyJobCanditateId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyJobCandidates"
    },
    recruiters: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyUsers"
    }],
    startDatetime:{
        type: Date
    },
    endDatetime: {
        type: Date
    },
    interviewAnswers:{
        type: Array
    },
});

module.exports = mongoose.model('companyJobInterviews', companyJobInterviewSchema);