const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyJobStatusHistorySchema = new mongoose.Schema({
    companyJobId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyJobs"
    },
    companyJobStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyJobStatuses"
    },
    createdBy: {
        type: String,
        required: false,
    },
    description:{
        type: String
    },
    creationDate:{
        type: Date
    }
});

module.exports = mongoose.model('companyJobStatusHistories', companyJobStatusHistorySchema);