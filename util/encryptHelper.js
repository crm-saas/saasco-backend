const bcrypt = require('bcrypt');

var hashPasswordAsync = async (password) => {
    try {
        const salt = bcrypt.genSaltSync(12);
        const hash = bcrypt.hashSync(password, salt);
        return hash;
    } catch (error) {
        console.error(error);
    }
}

var decryptPasswordAsync = async (password, hash) => {
    try {
        const match = bcrypt.compareSync(password, hash);
        return match;
    } catch (error) {
        console.error(error);
    }
}

module.exports= { hashPasswordAsync, decryptPasswordAsync }
