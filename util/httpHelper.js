const request = require('request-promise');

async function httpGet(url) {
    var options = {
        uri: url,
        json: true,
        headers: {
            "Accept": "application/json",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "en-US,en;q=0.9",
            "Connection": "keep-alive",
            "Content-Type": "application/json",
            "X-Requested-With": "XMLHttpRequest"
        }
    }
    var response = await request.get(options, async function (error, response, body) {
        if (!error && response.statusCode == 200) {
            return body;
        }
    });
    return response;
}

module.exports = { httpGet }