require('dotenv').config();
const { BlobServiceClient } = require('@azure/storage-blob');

const blobServiceClient = BlobServiceClient.fromConnectionString(process.env.CONNECTION_STRING);

const getBlobName = originalName => {
    const identifier = Math.random().toString().replace(/0\./, '');
    return `${identifier}-${originalName}.jpg`;
}

async function uploadContent(buffer) {
    const blobName = getBlobName('file');
    let image = Buffer.from(buffer.replace('data:image/png;base64,', ''), 'base64')
    const containerClient = blobServiceClient.getContainerClient(process.env.CONTAINER_NAME);
    const blockBlobClient = containerClient.getBlockBlobClient(blobName);

    let response = await blockBlobClient.uploadData(image, {
        blobHttpHeaders: { blobContentType: 'image/jpg' }
    });
    return response ? { response: response, fileName: blobName, fileUrl: `${process.env.CDN_URL}${blobName}`, created: true } : { response: response, created: false };
}

async function updateContent(blobName, buffer) {
    let image = Buffer.from(buffer.replace('data:image/png;base64,', ''), 'base64')
    const containerClient = blobServiceClient.getContainerClient(process.env.CONTAINER_NAME);
    const blockBlobClient = containerClient.getBlockBlobClient(blobName);
    let response = await blockBlobClient.uploadData(image, {
        blobHttpHeaders: { blobContentType: 'image/jpg' }
    });
    return response ? { response: response, fileName: blobName, fileUrl: `${process.env.CDN_URL}${blobName}`, updated: true } : { response: response, updated: false };
}

module.exports = { uploadContent, updateContent }