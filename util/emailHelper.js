const nodemailer = require("nodemailer");

async function sendEmail(body) {
    //let testAccount = await nodemailer.createTestAccount();

    let transporter = nodemailer.createTransport({
        host: "smtp.mailtrap.io",
        port: 2525,
        secure: false,
        auth: {
            user: "fb9af51395f0de",
            pass: "f3b827e0eb13cc"
        },
    });

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: 'talentu <juandavidv4@gmail.com>', // sender address
        to: body.recipient, // list of receivers
        subject: body.subject, // Subject line
        text: body.text, // plain text body
        html: "<a>" + body.url + "</a>", // html body
    });

    console.log("Message sent: %s", info.messageId);
}

module.exports = { sendEmail };