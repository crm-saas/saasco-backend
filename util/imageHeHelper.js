const request = require('request-promise').defaults({ encoding: null });

async function encodeImage(image) {
    return Buffer.from(image);
}

async function decodeImage(image) {
    return image.toString('base64');
}

async function decodeUrlImage(url) {
    var image = null;
    await request.get(url, async function (error, response, body) {
        if (!error && response.statusCode == 200) {
            data = "data:" + response.headers["content-type"] + ";base64," + Buffer.from(body).toString('base64');
            return image = data;
        }
    });
    return image
}

module.exports = {encodeImage,decodeImage,decodeUrlImage}