const mongoose = require('mongoose');
mongoose.pluralize(null);


const user_NotificationSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    userStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "notificationTypes"
    },
    message: {
        type: String,
    },
    isRead: {
        type: Boolean,
    },
    creationDate: {
        type: Date,
        default: Date.now,
    }
});

module.exports = mongoose.model('userNotifications', user_NotificationSchema);