const mongoose = require('mongoose');
mongoose.pluralize(null);


const userProfileEducationSchema = new mongoose.Schema({
    userProfileId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "userProfiles"
    },
    title: {
        type: String,
    },
    university: {
        type: String,
    },
    startDate: {
        type: Date,
    },
    endDate: {
        type: Date,
    },
    description: {
        type: String,
    },
});

module.exports = mongoose.model('userProfileEducations', userProfileEducationSchema);