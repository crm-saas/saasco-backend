const mongoose = require('mongoose');
mongoose.pluralize(null);


const userProfileExperienceSchema = new mongoose.Schema({
    userProfileId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "userProfiles"
    },
    position: {
        type: String,
    },
    company: {
        type: String,
    },
    startDate: {
        type: Date,
    },
    endDate: {
        type: Date,
    },
    description: {
        type: String,
    },
});

module.exports = mongoose.model('userProfileExperiences', userProfileExperienceSchema);