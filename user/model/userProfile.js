const mongoose = require('mongoose');
mongoose.pluralize(null);


const user_ProfileSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    locationId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "locations"
    },
    phone: {
        type: String,
    },
    linkedIn: {
        type: String,
    },
    website: {
        type: String,
    },
    bio: {
        type: String,
    },
    weeklyHoursAvailability: {
        type: Number,
    },
    skills: {
        type: Array,
    },
    
});

module.exports = mongoose.model('userProfiles', user_ProfileSchema);