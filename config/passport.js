const Strategy = require('passport-google-oauth20');
const authBL = require('../auth/model/user');
const config = require('../config/configs');

module.exports = function (passport) {
  passport.use(
    new Strategy(
      {
        clientID: process.env.GOOGLE_CLIENT_ID || config.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET || config.GOOGLE_CLIENT_SECRET,
        callbackURL: 'https://saascowebsite.azurewebsites.net/auth/google/callback',
      },
      async (accessToken, refreshToken, profile, done) => {
        const user = {
          googleid: profile.id,
          firstname: profile.name.givenName,
          lastname: profile.name.familyName,
          avatar: profile.photos[0].value,
          email: profile.emails[0].value,
          password: profile.password,
          enabled: true
        };

        try {
          let userInfo = await authBL.findUserByOne({ googleid: profile.id });

          if (userInfo) {
            done(null, userInfo);
          } else {
            userInfo = await authBL.registerUser(user);
            done(null, userInfo);
          }
        } catch (err) {
          console.error(err);
        }
      }
    )
  );

  // used to serialize the user for the session
  passport.serializeUser((user, done) => {
    done(null, user.id)
  });

  // used to deserialize the user
  passport.deserializeUser((id, done) => {
    let user = authBL.FindUserById(id);
    done(err, user);
  });
};