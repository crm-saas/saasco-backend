const express = require('express');
const middleware = require('../../middleware/auth');
const companyBL = require('../domain/company');
const jwt = require('jsonwebtoken');
const router = express.Router();

router.post('/', middleware.verifyToken, async (req, res) => {
    let host = req.headers.host;
    let token = req.header('authorization').replace('Bearer ', '')
    let userOwnerId = jwt.decode(token);
    let response = await companyBL.createCompany(req.body, userOwnerId.user, host);
    if (response.created) {
        let companyInfo = await companyBL.getCompany({ _id: response._id });
        let companyModel = await companyBL.generateJsonObject(companyInfo);
        res.status(201).json({ company: companyModel, message: "Company created successfully" });
    } else {
        res.status(500).json({
            message: "There is an error creating the company",
        });
    }
});

router.get('/information', middleware.verifyToken, (req, res) => {

});

router.post('/accept-invitation', middleware.verifyToken, async (req, res) => {
    let host = req.headers.host;
    let token = req.header('authorization').replace('Bearer ', '')
    let companyUser = jwt.decode(token);
    let responseValidation = await companyBL.validateInvitation(token);
    if (responseValidation.valid) {
        let response = await companyBL.acceptInvitation(companyUser, host, responseValidation.inviteInfo);
        if (response) {
            res.status(204).send({});
        } else {
            res.status(500).json({
                message: "There is an error processing the data",
            });
        }
    } else {
        res.status(404).json({
            message: "The invitation is expired",
        });
    }

});

router.get('/user-roles',middleware.verifyToken, async (req, res) => {
    let response = await companyBL.getCompanyUserRoles();
    if (response) {
        let companyUserRoles = response.data.map((value) => {
            return { id: value._id, name: value.name };
        });
        res.status(200).json(companyUserRoles);
    } else {
        res.status(404).json(response);
    }
});

module.exports = router;