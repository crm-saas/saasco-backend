const mongoose = require('mongoose');
const companyDAL = require('../model/company');
const companyUserDAL = require('../model/company_user');
const companyUserStatusHistoryDAL = require('../model/company_user_status_history');
const companyUserStatusDAL = require('../model/company_user_status');
const companyUserRoleDAL = require('../model/company_user_role');
const companyInviteDAL = require('../model/company_invite');
const inviteDAL = require('../model/company_invite');
const imageHelper = require('../../util/imageHeHelper');
const emailHelper = require('../../util/emailHelper');
const mediaHelper = require('../../util/mediaHelper');
const config = require('../../config/configs');
const jwt = require('jsonwebtoken');
const http = require('../../util/httpHelper');

async function createCompany(company, userOwnerId, host) {
    let avatarGeneratedByStorage= await mediaHelper.uploadContent(company.avatar);
    let avatarUrl= avatarGeneratedByStorage?.fileUrl;
    const newCompany = {
        name: company.name,
        description: company?.description ?? "",
        employees: company?.employees ?? "",
        contact: company?.contact ?? "",
        website: company?.website ?? "",
        avatar: avatarUrl,
        industryId: mongoose.Types.ObjectId(company.industry),
        locationId: mongoose.Types.ObjectId(company.location),
        ownerUserId: mongoose.Types.ObjectId(userOwnerId),
    }

    try {
        let InsertResponse = await companyDAL.create(newCompany);
        let { ...response } = JSON.parse(JSON.stringify(InsertResponse));
        sendInvitations(company, { ...response }, host);
        return response != undefined ? { created: true, ...response } : { created: false, response: "" };
    }
    catch (err) {
        console.error(err);
    }
}

async function updateCompany(company) {
    const companyToUpdate = {
        name: company.name,
        description: company?.description,
        employees: company?.employees,
        contact: company?.contact,
        website: company?.website,
        avatar: String(company.avatar).includes('http') ? company.avatar : await imageHelper.encodeImage(company.avatar),
        industryId: mongoose.Types.ObjectId(company.industry),
        locationId: mongoose.Types.ObjectId(company.location),
        ownerUserId: mongoose.Types.ObjectId(company.userId),
    };

    try {
        let response = await companyDAL.updateOne({
            "_id": mongoose.Types.ObjectId(company.id)
        },
            companyToUpdate
        );
        return !response ? { created: false, response: response } : { created: true, response: "" };
    }
    catch (err) {
        console.error(err);
    }
}

async function findCompanyUserStatusByOne(filter) {
    let companyUserStatusInfo = await companyUserStatusDAL.findOne(filter);
    return { "data": companyUserStatusInfo };
}

async function getCompany(companyId) {
    try {
        let companyInfo = await companyDAL.findById(companyId, function (err, company) {
            return company;
        });
        return companyInfo;
    }
    catch (err) {
        console.error(err);
    }
}

async function sendInvitations(companyMembers, company, host) {
    try {
        //migrate to microservices
        let inviting = await http.httpGet('https://' + host + '/auth/user/' + company.ownerUserId);
        //------------------------
        companyMembers.members.forEach(async (member) => {
            const Invitationpayload = {
                company: { id: company._id, name: company.name },
                email: member.email,
                role: member.role,
                invitedBy: inviting.name
            };
            let Invitationtoken = jwt.sign(Invitationpayload, config.KEY, {
                expiresIn: 3600
            });
            const payload = {
                company: { id: company._id, name: company.name, avatar: company.avatar },
                email: member.email,
                role: member.role,
                invitedBy: inviting.name,
                token: Invitationtoken
            };
            let token = jwt.sign(payload, config.KEY, {
                expiresIn: 3600
            });
            let bodyMessage = {
                subject: "Invitation to process",
                text: "You are invited to process",
                recipient: member.email,
                url: config.INVITEURL + token
            }
            const invite = { companyId: company._id, name: companyMembers.name, email: member.email, role: member.role, token: Invitationtoken, invitedBy: company.ownerUserId };
            await saveCompanyInvite(invite, 3600);
            await emailHelper.sendEmail(bodyMessage);
        });
    }
    catch (err) {
        console.error(err);
    }
}

async function saveCompanyInvite(inviteInfo, seconds) {
    let date = new Date();
    date.setSeconds(seconds);
    let inviteDto = {
        companyId: inviteInfo.companyId,
        email: inviteInfo.email,
        companyUserRoleId: inviteInfo.role,
        token: inviteInfo.token,
        expireDate: date,
        invitedBy: inviteInfo.invitedBy
    }
    try {
        await inviteDAL.create(inviteDto);
    }
    catch (err) {
        console.error(err);
    }
}

async function deleteCompanyInvite(inviteInfo) {
    try {
        await companyInviteDAL.deleteOne({
            "_id": mongoose.Types.ObjectId(inviteInfo[0]._id)
        });
    }
    catch (err) {
        console.error(err);
    }
}

async function getCompanyUserRoles() {
    try {
        let companyUserRoleInfo = await companyUserRoleDAL.find({}).select('_id name');
        return { data: companyUserRoleInfo };
    }
    catch (err) {
        console.error(err);
    }
}

async function validateInvitationData(invitationInfo, tokenInvitationInfo, invitationtoken) {
    const currentDate = new Date().getTime();
    const expireDate = tokenInvitationInfo.exp;
    let response = false;
    if (invitationtoken == invitationInfo[0].token) {
        if (expireDate <= currentDate) {
            response = true;
        } else {
            response = false;
        }
    } else {
        response = false;
    }
    return response;
}

async function validateInvitation(token) {
    let info = jwt.decode(token);
    const filter = {
        email: info.email,
        companyId: mongoose.Types.ObjectId(info.company.id)
    };
    const sort = {
        createdAt: -1
    };
    let response = await companyInviteDAL.find(filter, { sort: sort }).select('_id email token status companyId companyUserRoleId');
    if (response && response.length > 0) {
        let responseValidation = await validateInvitationData(response, info, token) ? true : false;
        return { valid: responseValidation, inviteInfo: response };
    } else {
        return { valid: false, inviteInfo: [] };
    }
}

async function acceptInvitation(companyUser, host, companyInvite) {
    let companyUserRoleId = companyUser.role;
    let userId = await getUserId(companyUser.email, host);
    let companyUserDto = {
        companyId: mongoose.Types.ObjectId(companyUser.company.id),
        userId: mongoose.Types.ObjectId(userId),
        companyUserRoleId: mongoose.Types.ObjectId(companyUserRoleId),
    }
    try {
        let responseCompanyUserCreation = await companyUserDAL.create(companyUserDto);
        await deleteCompanyInvite(companyInvite);
        await addCompanyUserStatusHistory(responseCompanyUserCreation, 'ACTIVE', 'Invitation Accepted');
        return true;
    }
    catch (err) {
        console.error(err);
        return false;
    }
}

//migrate to microservices
async function getUserId(filter, host) {
    let user = await http.httpGet('https://' + host + '/auth/user/email/' + filter);
    return user.id;
}
//-------------------------------

async function addCompanyUserStatusHistory(companyUserInfo, status, description) {
    let companyUserStatusId = await findCompanyUserStatusByOne({ key: status });
    let companyUserStatusHistoryDTO = {
        companyUserId: mongoose.Types.ObjectId(companyUserInfo._id),
        companyUserStatusId: mongoose.Types.ObjectId(companyUserStatusId.data._id),
        description: description,
    }
    try {
        await companyUserStatusHistoryDAL.create(companyUserStatusHistoryDTO);
    }
    catch (err) {
        console.error(err);
    }
}

async function generateJsonObject(companyModel) {
    let companyDto = {
        id: companyModel._id,
        name: companyModel.name,
        locationId: companyModel.location_id,
        industryId: companyModel.industry_id,
        description: companyModel.description,
        employees: companyModel.employees ?? 0,
        contact: companyModel.contact,
        website: companyModel.website,
        avatar: companyModel.avatar != null ? String(companyModel.avatar).includes('http') ? companyModel.avatar : await imageHelper.decodeImage(companyModel.avatar) : null
    };
    return companyDto;
}

module.exports = { createCompany, updateCompany, getCompany, generateJsonObject, sendInvitations, acceptInvitation, validateInvitation, getCompanyUserRoles }