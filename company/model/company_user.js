const mongoose = require('mongoose');
mongoose.pluralize(null);

const company_userSchema = new mongoose.Schema({
    companyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companies"
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    companyUserRoleId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyUserRoles"
    }
});

module.exports = mongoose.model('companyUsers', company_userSchema);