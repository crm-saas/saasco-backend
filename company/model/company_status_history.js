const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyStatusHistorySchema = new mongoose.Schema({
    "companyId": {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companies"
    },
    "companyStatusId": {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyStatuses"
    },
    "createdBy": {
        type: String,
    },
    "description": {
        type: String,
    },
    "creationDate": {
        type: Date,
    },
});

module.exports = mongoose.model('companyStatusHistories', companyStatusHistorySchema);