const mongoose = require('mongoose');

const companySchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    description: {
        type: String,
        required: false
    },
    employees: {
        type: Number,
        required: false,
    },
    contact: {
        type: String,
        required: false,
    },
    website: {
        type: String,
        required: false,
    },
    avatar: {
        type: String,
        required: false,
    },
    industryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyIndustries"
    },
    locationId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "locations"
    },
    ownerUserId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    }
});

module.exports = mongoose.model('companies', companySchema);