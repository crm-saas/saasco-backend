const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyUserNotificationSettingsSchema = new mongoose.Schema({
    companyUserId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyUsers"
    },
    newJobApplication: {
        type: Boolean
    },
    jobApplicationInviteAccepted: {
        type: Boolean
    },
    jobInterviewAccepted: {
        type: Boolean
    },
    jobInterviewRescheduled: {
        type: Boolean
    },
    companyInviteAccepted: {
        type: Boolean
    },
    addedToJobInterview: {
        type: Boolean
    }
});

module.exports = mongoose.model('companyUserNotificationSettings', companyUserNotificationSettingsSchema);