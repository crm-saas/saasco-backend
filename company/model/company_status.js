const mongoose = require('mongoose');
mongoose.pluralize(null);


const companyStatusesSchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('companyStatuses', companyStatusesSchema);