const mongoose = require('mongoose');
mongoose.pluralize(null);


const company_user_roleSchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('companyUserRoles', company_user_roleSchema);