const mongoose = require('mongoose');
mongoose.pluralize(null);


const company_user_status_historySchema = new mongoose.Schema({
    companyUserId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyUsers"
    },
    companyUserStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyUserStatuses"
    },
    createdBy: {
        type: String,
        default: "test",
    },
    description: {
        type: String,
    },
    createdDate: {
        type: Date,
        default: Date.now,
    }
});

module.exports = mongoose.model('companyUserStatusHistories', company_user_status_historySchema);