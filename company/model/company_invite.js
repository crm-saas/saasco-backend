const mongoose = require('mongoose');
mongoose.pluralize(null);


const company_inviteSchema = new mongoose.Schema({
    companyId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companies"
    },
    companyUserRoleId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "companyUserRoles"
    },
    invitedBy: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    email: {
        type: String,
        required: false,
    },
    token: {
        type: String,
        required: false,
    },
    expire_date: {
        type: Date,
        required: false,
    },
    createdAt: {
        type: Date,
        default: Date.now,
    },
});

module.exports = mongoose.model('companyInvites', company_inviteSchema);