const mongoose = require('mongoose');
mongoose.pluralize(null);


const company_user_statusSchema = new mongoose.Schema({
    "key": {
        type: String,
        required: true,
    },
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('companyUserStatuses', company_user_statusSchema);