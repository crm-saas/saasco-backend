const locationDAL = require('../model/location');
const IndustryDAL = require('../model/industry');

async function getLocations() {
    try {
        let locations = await locationDAL.find({}).select('_id name');
        return { data: locations };
    }
    catch (err) {
        console.error(err);
    }
}

async function getIndustries() {
    try {
        let IndustryInfo = await IndustryDAL.find({}).select('_id name');
        return { data: IndustryInfo };
    }
    catch (err) {
        console.error(err);
    }
}

module.exports = { getLocations, getIndustries }