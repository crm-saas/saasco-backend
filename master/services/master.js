const express = require('express');
const middleware = require('../../middleware/auth');
const masterListBL = require('../domain/master_list');
const router = express.Router();

router.get('/locations', middleware.verifyToken, async (req, res) => {
    let response = await masterListBL.getLocations();
    if (response) {
        let locations = response.data?.map((value) => {
            return { id: value._id, name: value.name };
        });
        res.status(200).json(locations);
    } else {
        res.status(404).json(response);
    }
});

router.get('/industries',middleware.verifyToken, async (req, res) => {
    let response = await masterListBL.getIndustries();
    if (response) {
        let industries = response.data?.map((value) => {
            return { id: value._id, name: value.name };
        });
        res.status(200).json(industries);
    } else {
        res.status(404).json(response);
    }
});

module.exports = router;