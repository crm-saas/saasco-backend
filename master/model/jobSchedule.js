const mongoose = require('mongoose');

const jobScheduleSchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('jobSchedules', jobScheduleSchema);