const mongoose = require('mongoose');

const notificationTypeSchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('notificationTypes', notificationTypeSchema);