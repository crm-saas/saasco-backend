const mongoose = require('mongoose');
mongoose.pluralize(null);


const skillSchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
    "relatedSkills":{
        type: Array
    }
});

module.exports = mongoose.model('skills', skillSchema);