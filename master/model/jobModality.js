const mongoose = require('mongoose');

const jobModalitySchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('jobModalities', jobModalitySchema);