const mongoose = require('mongoose');
mongoose.pluralize(null);


const industrySchema = new mongoose.Schema({
    "name": {
        type: String,
        required: true,
    },
    "subIndustries":{
        type: Array
    }
});

module.exports = mongoose.model('companyIndustries', industrySchema);