const jwt = require('jsonwebtoken');
const config = require('../config/configs');

function ensureAuth(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    } else {
        res.redirect('/');
    }
}

function ensureGuest(req, res, next) {
    if (!req.isAuthenticated()) {
        return next();
    } else {
        res.redirect('/log');
    }
}

function verifyToken(req, res, next) {
    const tokenHeader = req.headers['authorization'];
    if (tokenHeader) {
        token = tokenHeader.replace('Bearer ', '')
        jwt.verify(token, config.KEY, (err, decoded) => {
            if (err) {
                return res.status(401).send({ message: 'Invalid token' });
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        res.status(400).send({
            message: 'Token not provided'
        });
    }
}


module.exports = { ensureAuth, ensureGuest, verifyToken }
