const express = require('express');
const { OAuth2Client } = require('google-auth-library');
const jwt = require('jsonwebtoken');
const middleware = require('../../middleware/auth');
const authBL = require('../domain/auth');
const config = require('../../config/configs');
const router = express.Router();
const client = new OAuth2Client(config.GOOGLE_CLIENT_ID);

/*--------------------------------------- code for linkedin integration
router.get('/google', passport.authenticate('google', { scope: ['profile', 'email'] }));


router.get('/google/callback', passport.authenticate('google', { failureRedirect: '/' }), async (req, res) => {
    console.log('/google/callback ' + req.user);
    const payload = await authBL.findUserByOne({ email: req.user.email });
    const token = jwt.sign(payload, config.KEY, {
        expiresIn: 1440
    });
    res.cookie('googleToken', token);
    res.status(200).send({ userinfo: req.user, token: token });
}); -------------------------------------------------------------------*/

router.get('/logout', (req, res) => {
    req.session.destroy();
    req.logout();
});

router.get('/', middleware.ensureGuest, (req, res) => {
    res.send('login');
});

router.get('/log', middleware.ensureAuth, async (req, res) => {
    res.send({ userinfo: req.user, token: token });
});

router.post('/login', async (req, res) => {
    let response = await authBL.validateUser(req.body.email, req.body.password, 'LOGIN');
    if (response.valid) {
        const payload = { user: response.user.id, name: response.user.name, email: response.user.email, role: response.user.role };
        const token = jwt.sign(payload, config.KEY, {
            expiresIn: 1440
        });
        res.json({
            user: response.user,
            token: token
        });
    } else {
        res.status(401).json({
            message: response.message,
        });
    }
});

router.post("/google", async (req, res) => {
    const { token, status } = req.body;
    const statusInfo = status !== undefined ? 'ACTIVE' : status;
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: config.GOOGLE_CLIENT_ID
    });
    const { email, given_name, family_name, picture } = ticket.getPayload();
    const user = await authBL.findUserByOne({ email: email })
    if (user.user) {
        let payload = { user: user.user.id, name: user.user.name, email: user.user.email, role: user.user.role };
        let userModel = await authBL.generateJsonObject(user.user, statusInfo, 'LOGIN');
        let token = jwt.sign(payload, config.KEY, {
            expiresIn: 1440
        });
        res.status(200).json({ user: userModel, token });
    } else {
        let userToInsert = { email, "password": '123456', "name": given_name + ' ' + family_name, "avatar": picture };
        let response = await authBL.registerUser(userToInsert);
        if (response.created) {
            let userInfo = await authBL.findUserByOne({ email: email });
            let userModel = await authBL.generateJsonObject(userInfo.user, statusInfo, 'REGISTER');
            let payload = { user: userInfo.user.id, name: userInfo.user.name, email: userInfo.user.email, role: userInfo.user.role };
            let token = jwt.sign(payload, config.KEY, {
                expiresIn: 1440
            });
            res.status(201).json({ user: userModel, token });
        }
    }
});

router.post('/register', async (req, res) => {
    let responseValidation = await authBL.findUserByOne({ email: req.body.email });
    if (responseValidation.user) {
        res.status(200).json({ message: "This email is already registered" });
    } else {
        let response = await authBL.registerUser(req.body);
        if (response.created) {
            let userInfo = await authBL.findUserByOne({ email: response.email });
            let userModel = await authBL.generateJsonObject(userInfo.user, req.body.status, 'REGISTER');
            let payload = { user: userInfo.user.id, name: userInfo.user.name, email: userInfo.user.email, role: userInfo.user.role };
            let token = jwt.sign(payload, config.KEY, {
                expiresIn: 1440
            });
            res.status(201).json({ user: userModel, token, message: "User created successfully" });
        } else {
            res.status(500).json({
                message: "There is an error creating the user",
            });
        }
    }
});

router.post('/password/recover', async (req, res) => {
    let user = await authBL.findUserByOne({ email: req.body.email });
    if (user.user) {
        authBL.RecoverPassword(user.user);
        res.status(200).json({ message: "We´ve sent a message with your recovery token" });
    } else {
        res.status(404).json({
            message: "The email is not registered",
        });
    }
});

router.put('/password/update', middleware.verifyToken, async (req, res) => {
    let token = req.header('authorization').replace('Bearer ', '');
    let userId = jwt.decode(token).id;
    let response = await authBL.updatePassword(req.body.password, userId)
    if (response.updated) {
        res.status(200).json({ message: "User update sucessfully" });
    } else {
        res.status(500).json({
            message: "There is an error updating the user",
        });
    }
});

router.get('/user/:id', async (req, res) => {
    let userId = req.params.id;
    let response = await authBL.findUserById(userId);
    if (response) {
        res.status(200).json(response);
    } else {
        res.status(404).json(response);
    }
})

router.get('/user/email/:email', async (req, res) => {
    let email = req.params.email;
    let response = await authBL.findUserByOne({ email: email });
    if (response) {
        let user = { ...response.user._doc };
        let json = { id: user._id, email: user.email, role: user.role };
        res.status(200).json(json);
    } else {
        res.status(404).json(response);
    }
})

module.exports = router;