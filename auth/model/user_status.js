const mongoose = require('mongoose');
mongoose.pluralize(null);

const user_statusSchema = new mongoose.Schema({
    "key": {
        type: String,
        required: true,
    },
    "name": {
        type: String,
        required: true,
    },
});

module.exports = mongoose.model('userStatuses', user_statusSchema);