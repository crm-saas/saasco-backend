const mongoose = require('mongoose');
mongoose.pluralize(null);


const user_status_historySchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "users"
    },
    userStatusId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "userStatuses"
    },
    createdBy: {
        type: String,
        default: "test",
    },
    description: {
        type: String,
    },
    creationDate: {
        type: Date,
        default: Date.now,
    }
});

module.exports = mongoose.model('userStatusHistories', user_status_historySchema);