const mongoose = require('mongoose');
const bcrypt = require('bcrypt');

const saltrounds = 12;

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String
    },
    name: {
        type: String,
        required: true,
    },
    avatar: {
        type: String
    },
    role: {
        type: String,
    },
    emailVerified: {
        type: Boolean
    }
});


userSchema.pre('save', function (next) {
    const user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(saltrounds, function (saltError, salt) {
            if (saltError) {
                return next(saltError);
            } else {
                bcrypt.hash(user.password, salt, function (hashError, hash) {
                    if (hashError) {
                        return next(hashError);
                    }
                    user.password = hash;
                    next();
                })
            }
        })
    } else {
        return next();
    }
});

userSchema.pre('updateOne', function (next) {
    const user = this;
    bcrypt.genSalt(saltrounds, function (saltError, salt) {
        if (saltError) {
            return next(saltError);
        } else {
            bcrypt.hash(user._update.password, salt, function (hashError, hash) {
                if (hashError) {
                    return next(hashError);
                }
                user._update.password = hash;
                next();
            })
        }
    })
});

module.exports = mongoose.model('users', userSchema)

