const userDAL = require('../model/user');
const userStatusHistoryDAL = require('../model/user_status_history');
const userStatusDAL = require('../model/user_status');
const encrypterHelper = require('../../util/encryptHelper');
const imageHelper = require('../../util/imageHeHelper');
const config = require('../../config/configs');
const jwt = require('jsonwebtoken');
const emailHelper = require('../../util/emailHelper');
const mongoose = require('mongoose');

async function validateUser(user, password) {
    let userInfo = await userDAL.findOne({ email: user });
    if (userInfo) {
        var passwordIsMatch = await encrypterHelper.decryptPasswordAsync(password, userInfo.password);
        if (userInfo.email == user && passwordIsMatch) {
            var user = await generateJsonObject(userInfo, null, 'LOGIN');
            return { "user": user, "valid": true, "message": 'Welcome' };
        } else {
            return { "user": '', "valid": false, "message": 'Invalid user or password' };
        }
    } else {
        return { "user": '', "valid": false, "message": 'User not registered' };
    }
}

async function findUserByOne(filter) {
    let userInfo = await userDAL.findOne(filter);
    return { "user": userInfo };
}

async function findUserById(id) {
    let userInfo = await userDAL.findById(id, function (err, user) {
        return user;
    });
    return userInfo;
}

async function findUserStatusByOne(filter) {
    let userStatusInfo = await userStatusDAL.find(filter);
    return { "userStatus": userStatusInfo };
}

async function registerUser(user) {
    const newUser = {
        email: user.email,
        password: user.password,
        name: user.name,
        avatar: null
    };

    try {
        let responseInsert = await userDAL.create(newUser);
        let { ...response } = JSON.parse(JSON.stringify(responseInsert));
        return response != undefined ? { created: true, ...response } : { created: false, response: "" };
    }
    catch (err) {
        console.error(err);
    }
}

async function getUserStatusHistory(userId) {
    const filter = {
        userId: mongoose.Types.ObjectId(userId)
    };
    const sort = {
        createdDate: -1
    };
    let response = await userStatusHistoryDAL.find(filter, { sort: sort }).select('_id userId userStatusId description createdDate');
    return response;
}

async function createUserStatusHistory(userInfo, status) {
    userStatusResponse = await findUserStatusByOne({ key: status !== undefined || null ? status : 'ACTIVE' })
    userStatusInfo = {
        userId: userInfo.id,
        userStatusId: userStatusResponse.userStatus[0].id,
        description: userStatusResponse.userStatus.key
    }
    try {
        let responseInsert = await userStatusHistoryDAL.create(userStatusInfo);
        let { ...response } = JSON.parse(JSON.stringify(responseInsert));
        return response != undefined ? { ...response } : { response: "" };
    }
    catch (err) {
        console.error(err);
    }
}

async function updateUser(user) {
    const userToUpdate = {
        email: user.email,
        password: user.password,
        name: user.name,
        avatar: String(user.avatar).includes('http') ? user.avatar : await imageHelper.encodeImage(user.avatar),
    };

    try {
        let response = await userDAL.updateOne({
            "_id": mongoose.Types.ObjectId(user.id)
        }, userToUpdate);
        return response != undefined ? { created: true, response: response } : { created: false, response: "" };
    }
    catch (err) {
        console.error(err);
    }
}

async function updatePassword(password, userId) {
    const passwordToUpdate = { password: password };

    try {
        let response = await userDAL.updateOne({
            "_id": mongoose.Types.ObjectId(userId)
        }, passwordToUpdate);
        return response != undefined ? { updated: true, response: response } : { updated: false, response: "" };
    }
    catch (err) {
        console.error(err);
    }
}

async function RecoverPassword(user) {
    const payload = { id: user._id, name: user.name, email: user.email };
    let token = jwt.sign(payload, config.KEY, {
        expiresIn: 2800
    });
    let bodyMessage = {
        subject: "Password Recovery",
        text: "You can recover your password",
        recipient: user.email,
        url: config.RECOVERPASSWORDURL + token
    }
    await emailHelper.sendEmail(bodyMessage);
}

async function generateJsonObject(userModel, status, process) {
    let roles = (await getUserRoles()).find((data) => {
        return data._id == userModel.id;
    });
    if (process === 'REGISTER') { await createUserStatusHistory(userModel, status) };
    let userStatusHistory = await getUserStatusHistory(userModel.id);
    return {
        id: userModel.id,
        email: userModel.email,
        name: userModel.name,
        role: { id: roles?.role?._id, name: roles?.role?.name },
        avatar: userModel.avatar != null ? String(userModel.avatar).includes('http') ? userModel.avatar : await imageHelper.decodeImage(userModel.avatar) : null,
        status: userStatusHistory[0]?.description
    }
}

async function getUserRoles() {
    let roles = await userDAL.aggregate([
        {
            $lookup: {
                from: "companyUserRoles",
                localField: "role",
                foreignField: "_id",
                as: "role"
            }
        },
        {
            $unwind: '$role'
        },
    ]);
    return roles;
}

module.exports = { validateUser, registerUser, findUserById, findUserByOne, updateUser, generateJsonObject, RecoverPassword, updatePassword }
