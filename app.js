const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const passport = require('passport');
const session = require('express-session');
const mongoStore = require('connect-mongo')(session);
const path = require('path');
const bodyParser = require('body-parser');
const cors= require('cors');

dotenv.config({path: path.resolve(__dirname,`${process.env.NODE_ENV}.env`)});
const port = process.env.PORT;

var app = express();
require('./config/passport')(passport);

mongoose.connect(process.env.MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

app.use(cors());

app.set('key', process.env.PORT);

app.use(express.urlencoded({ limit:'50mb', extended: true }));
app.use(bodyParser.json({limit: '50mb'}));

app.use(session({
    secret: 'secret',
    resave: false,
    saveUninitialized: false,
    store: new mongoStore({
        mongooseConnection: mongoose.connection,
        ttl: 24 * 60 * 60 * 1000,
        autoRemove: 'interval',
        autoRemoveInterval: 10
    })
}));

//passpor middleware
app.use(passport.initialize());
app.use(passport.session());

//services
app.use('/auth', require('./auth/services/auth'));
app.use('/company', require('./company/services/company'));
app.use('/master', require('./master/services/master'));


//server initialization
app.listen(port, console.log(`listening at ${port}`))
